#include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)


# PID file will store the server process id when it's running on development mode
NATS_PID=/tmp/.$(PROJECTNAME)-nats-server.pid

# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";


start-nats:
	@echo "start nats jetstream server"
	nats -js 2>&1 & echo $$! > $(NATS_PID)
	@cat $(NATS_PID) | sed "/^/s/^/  \>  PID: /"

stop-nats:
	@-touch $(NATS_PID)
	@-kill `cat $(NATS_PID)` 2> /dev/null || true
	@-rm $(NATS_PID)


tests:
	#go test bitbucket.org/orangeparis/snifferlog/internal/processors
	#go test bitbucket.org/orangeparis/snifferlog/players


compile:
	echo "Compiling for every OS and Platform"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/feeder ./cmd/feeder/feeder.go
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/feeder ./cmd/feeder/feeder.go
	cp  build/packages/linux_amd64/feeder /Volumes//keybase/team/e2e_ines/feeder_V0.2

