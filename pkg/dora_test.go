package feeders

import (
	"context"
	"fmt"
	"testing"
	"time"

	model "bitbucket.org/orangeparis/feeders"
	"github.com/nats-io/nats.go"
)

func TestDoraFeeder(t *testing.T) {

	lb := &model.LiveboxInfo{
		ID:     "LB1",
		Fti:    "fti/abcdef",
		Mac:    "mac",
		LineID: "LineID",
	}
	_ = lb

	// creates nats connection
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", nats.DefaultURL)
		t.Fail()
		return
	}
	//defer nc.Close()

	// create cancel function from context
	ctx, cancel := context.WithCancel(context.TODO())

	// create a dora feeder
	f := NewDoraFeeder(nc, lb)

	// run it
	f.Run(ctx)

	// intercept first log message
	sub, err := nc.SubscribeSync(f.log)
	m, err := sub.NextMsg(2 * time.Second)
	if err != nil {
		t.Fail()
		cancel()
		return
	}
	sub.Unsubscribe()
	fmt.Printf("message received from log: %s\n", m.Data)

	// send a fake message on sink and catch it
	sub2, err := nc.SubscribeSync(f.sink)
	err = nc.Publish(f.sink, []byte("test message"))
	m2, err := sub2.NextMsg(1 * time.Second)
	if err != nil {
		t.Fail()
		cancel()
		return
	}
	sub2.Unsubscribe()
	fmt.Printf("message received from sink: %s\n", m2.Data)

	// cancel all goroutines for this context
	time.Sleep(3 * time.Second)
	cancel()
	time.Sleep(2 * time.Second)
	//nc.Close()
	println("Done.")
}
