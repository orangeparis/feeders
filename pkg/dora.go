package feeders

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"

	model "bitbucket.org/orangeparis/feeders"
)

/*


	Dora feeder

	a proccess listening to 	event.livebox.fti.<fti>.dora
	and publish to stream 		SESSION.<LiveboxID>.dora
	also publish log on 		SESSION.<LiveboxID>.log


	usage:

	f := NewDoraFeeder(natsConnection, liveboxInfo )
	f.Run(ctx)

*/

// DoraFeeder publisher
type DoraFeeder struct {
	nc      *nats.Conn
	sub     *nats.Subscription
	source  string
	sink    string
	log     string // log subject
	livebox *model.LiveboxInfo
	channel chan *nats.Msg
}

// NewDoraFeeder creates a nes Dora aggregator
func NewDoraFeeder(nc *nats.Conn, l *model.LiveboxInfo) *DoraFeeder {

	source := fmt.Sprintf("event.livebox.fti.%s.dora", l.Fti)
	sink := fmt.Sprintf("SESSION.%s.dora", l.ID)
	log := fmt.Sprintf("SESSION.%s.log", l.ID)

	f := &DoraFeeder{
		nc:      nc,
		livebox: l,
		source:  source,
		sink:    sink,
		log:     log,
	}

	return f

}

// Run Starts DoraFeeder
func (x *DoraFeeder) Run(ctx context.Context) {

	// Channel Subscriber
	ch := make(chan *nats.Msg, 64)
	sub, err := x.nc.ChanSubscribe(x.source, ch)
	if err != nil {
		fmt.Printf("Dora Feeder failed to subscribe: %s\n", err.Error())
		return
	}
	fmt.Printf("Dora Feeder listen to : %s\n", x.source)
	fmt.Printf("Dora Feeder write to : %s\n", x.sink)

	go func() {
		fmt.Printf("Dora Feeder started\n")
		x.nc.Publish(x.log, []byte(fmt.Sprintf("Dora feeder starts listening on %s", x.source)))
		for {

			select {

			case msg := <-ch:
				// publish received message on sink
				err := x.nc.Publish(x.sink, msg.Data)
				if err != nil {
					fmt.Printf("Dora Feeder failed to publish: %s\n", err.Error())
				}

			case <-ctx.Done():
				// receive Done from ancestor
				// shutdown feeder
				sub.Unsubscribe()
				fmt.Printf("Dora Feeder is closed\n")
				x.nc.Publish(x.log, []byte("Dora feeder exited\n"))
				return
			}
		}

	}()

}
