package feeders

import (
	"fmt"
	"testing"
	"time"

	"github.com/nats-io/nats.go"
)

func TestManager(t *testing.T) {

	// creates nats connection
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", nats.DefaultURL)
		t.Fail()
		return
	}
	defer nc.Close()

	// create stream SESSION.>
	stream := CreateStream(nc)
	_ = stream

	time.Sleep(1 * time.Second)

	// create cancel function from context
	//ctx, cancel := context.WithCancel(context.TODO())

	// publish fake messages on SESSION.LB1.dora
	err = nc.Publish("SESSION.LB1.dora", []byte("test dora session message 1"))
	err = nc.Publish("SESSION.LB1.dora", []byte("test dora session message 2"))
	err = nc.Publish("SESSION.LB1.dora", []byte("test dora session message 3"))

	// wait a while
	time.Sleep(3 * time.Second)

	// set up consumer to read SESSION.LB1.> from persistent stream
	subject := fmt.Sprintf("SESSION.LB1.>")
	consumer := CreateConsumer(nc, subject)

	msg, err := consumer.NextMsg()
	if err == nil {
		//meta, _ := jsm.ParseJSMsgMetadata(msg)
		fmt.Printf("stream   consumer received msg: %s\n", msg.Data)
	} else {
		fmt.Printf("stream   failed to receive message: %s\n", err)
	}
	_ = msg

	time.Sleep(2 * time.Second)
	//nc.Close()
	println("Done.")

}
