package feeders

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"

	model "bitbucket.org/orangeparis/feeders"
)

/*


	applog feeder

	listen to sniffer.*.<mac>.log.>

	   eg sniffer.log.0.78:94:b4:3d:2a:78.dhcpv6.91

	publish  to SESSION.<LiveboxID>.applog

	also publish log on 		SESSION.<LiveboxID>.log

	usage:

	f := NewAppLogFeeder(natsConnection, liveboxInfo )
	f.Run(ctx)

*/

// AppLogFeeder publisher
type AppLogFeeder struct {
	nc      *nats.Conn
	sub     *nats.Subscription
	source  string
	sink    string
	log     string // log subject
	livebox *model.LiveboxInfo
	channel chan *nats.Msg
}

// NewAppLogFeeder creates a nes Dora aggregator
func NewAppLogFeeder(nc *nats.Conn, l *model.LiveboxInfo) *AppLogFeeder {

	source := fmt.Sprintf("sniffer.log.%s", l.LineID)
	sink := fmt.Sprintf("SESSION.%s.applog", l.ID)
	log := fmt.Sprintf("SESSION.%s.log", l.ID)

	f := &AppLogFeeder{
		nc:      nc,
		livebox: l,
		source:  source,
		sink:    sink,
		log:     log,
	}

	return f

}

// Run Starts DhcpV6Feeder
func (x *AppLogFeeder) Run(ctx context.Context) {

	// Channel Subscriber
	ch := make(chan *nats.Msg, 64)
	sub, err := x.nc.ChanSubscribe(x.source, ch)
	if err != nil {
		fmt.Printf("AppLog Feeder failed to subscribe: %s\n", err.Error())
		return
	}
	fmt.Printf("AppLog Feeder listen to : %s\n", x.source)
	fmt.Printf("AppLog Feeder write to : %s\n", x.sink)

	go func() {
		fmt.Printf("AppLog Feeder started\n")
		x.nc.Publish(x.log, []byte(fmt.Sprintf("AppLog feeder starts listening on %s", x.source)))
		for {

			select {

			case msg := <-ch:
				// publish received message on sink
				err := x.nc.Publish(x.sink, msg.Data)
				if err != nil {
					fmt.Printf("AppLog Feeder failed to publish: %s\n", err.Error())
				}

			case <-ctx.Done():
				// receive Done from ancestor
				// shutdown feeder
				sub.Unsubscribe()
				fmt.Printf("AppLog Feeder is closed\n")
				x.nc.Publish(x.log, []byte("AppLog feeder exited\n"))
				return
			}
		}

	}()

}
