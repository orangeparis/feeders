package feeders

import (
	"time"

	"github.com/nats-io/jsm.go"
	"github.com/nats-io/nats.go"
)

/*

	manager

	tools to create stream and consumer for SESSION.*



*/

// CreateStream Create the stream SESSION.>
func CreateStream(nc *nats.Conn) *jsm.Stream {

	mgr, _ := jsm.New(nc)
	stream, err := mgr.NewStream(
		"SESSION",
		jsm.Subjects("SESSION.>"),
		jsm.MaxAge(360*time.Second),
		jsm.FileStorage(),
	)
	_ = err
	return stream

}

// CreateConsumer Create the stream SESSION.LB1.>
func CreateConsumer(nc *nats.Conn, subject string) *jsm.Consumer {

	mgr, _ := jsm.New(nc)

	consumer, err := mgr.NewConsumer(
		"SESSION",
		jsm.FilterStreamBySubject(subject), // "SESSION.LB1.>"
		jsm.DurableName("SESSIONDATA"),
	)
	_ = err
	// consumer, err := mgr.NewConsumer(
	// 	"SESSION",
	// 	"ALL",
	// 	jsm.FilterSubject("SESSION.>"),
	// 	jsm.SampleFrequency("100"),
	// )
	return consumer

}
