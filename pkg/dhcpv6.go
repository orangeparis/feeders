package feeders

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"

	model "bitbucket.org/orangeparis/feeders"
)

/*


	dhcpv6 feeder

	listen to sniffer.<mac>.dhcpv6.<type>.<sequence>

	   eg sniffer.0.78:94:b4:3d:2a:78.dhcpv6.91

	publish  to SESSION.<LiveboxID>.dhcpv6

	also publish log on 		SESSION.<LiveboxID>.log

	usage:

	f := NewDhcpV6Feeder(natsConnection, liveboxInfo )
	f.Run(ctx)

*/

// DhcpV6Feeder publisher
type DhcpV6Feeder struct {
	nc      *nats.Conn
	sub     *nats.Subscription
	source  string
	sink    string
	log     string // log subject
	livebox *model.LiveboxInfo
	channel chan *nats.Msg
}

// NewDhcpV6Feeder creates a nes Dora aggregator
func NewDhcpV6Feeder(nc *nats.Conn, l *model.LiveboxInfo) *DhcpV6Feeder {

	source := fmt.Sprintf("sniffer.*.%s.dhcpv6.>", l.Mac)
	sink := fmt.Sprintf("SESSION.%s.dhcpv6", l.ID)
	log := fmt.Sprintf("SESSION.%s.log", l.ID)

	f := &DhcpV6Feeder{
		nc:      nc,
		livebox: l,
		source:  source,
		sink:    sink,
		log:     log,
	}

	return f

}

// Run Starts DhcpV6Feeder
func (x *DhcpV6Feeder) Run(ctx context.Context) {

	// Channel Subscriber
	ch := make(chan *nats.Msg, 64)
	sub, err := x.nc.ChanSubscribe(x.source, ch)
	if err != nil {
		fmt.Printf("DhcpV6 Feeder failed to subscribe: %s\n", err.Error())
		return
	}
	fmt.Printf("DhcpV6 Feeder listen to : %s\n", x.source)
	fmt.Printf("DhcpV6 Feeder write to : %s\n", x.sink)

	go func() {
		fmt.Printf("DhcpV6 Feeder started\n")
		x.nc.Publish(x.log, []byte(fmt.Sprintf("DhcpV6 feeder starts listening on %s", x.source)))
		for {

			select {

			case msg := <-ch:
				// publish received message on sink
				err := x.nc.Publish(x.sink, msg.Data)
				if err != nil {
					fmt.Printf("DhcpV6 Feeder failed to publish: %s\n", err.Error())
				}

			case <-ctx.Done():
				// receive Done from ancestor
				// shutdown feeder
				sub.Unsubscribe()
				fmt.Printf("DhcpV6 Feeder is closed\n")
				x.nc.Publish(x.log, []byte("DhcpV6 feeder exited\n"))
				return
			}
		}

	}()

}
