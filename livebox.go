package model

import (
	"context"
)

// LiveboxInfo a livebox set of data
type LiveboxInfo struct {
	ID     string // Livebox identifier
	Fti    string // fti
	Mac    string // mac
	LineID string // Line ID

}






// Feeder interface
type Feeder interface {
	Run(ctx context.Context)
}
