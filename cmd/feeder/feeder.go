package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"sync"

	model "bitbucket.org/orangeparis/feeders"
	session "bitbucket.org/orangeparis/feeders/pkg"
	"github.com/nats-io/nats.go"
	//"bitbucket.org/orangeparis/ines/heartbeat"
	//"bitbucket.org/orangeparis/lbservices/config"
	//"bitbucket.org/orangeparis/lbservices/dora"
	//"github.com/jirenius/go-res"
)

/*
	runs a feeder

	listen to

	*


	// LiveboxInfo a livebox set of data
	type LiveboxInfo struct {
		ID     string // Livebox identifier
		Fti    string // fti
		Mac    string // mac
		LineID string // Line ID

	}


*/

var natsURL string
var liveboxID string
var liveboxFti string
var liveboxMac string
var liveboxLineID string

func main() {

	flag.StringVar(&natsURL, "nats", "nats://127.0.0.1:4222", "nats server url")
	flag.StringVar(&liveboxID, "ID", "", "Livebox ID")
	flag.StringVar(&liveboxFti, "fti", "", "Livebox fti")
	flag.StringVar(&liveboxMac, "mac", "", "Livebox mac")
	flag.StringVar(&liveboxLineID, "line", "", "Livebox Line ID")

	flag.Parse()

	// check parameters
	if liveboxID == "" {
		fmt.Printf("ID (liveboxID) is missing\n")
		flag.Usage()
		return
	}

	if liveboxMac == "" {
		fmt.Printf("Mac (liveboxMac) is missing\n")
		flag.Usage()
		return
	}

	// set the global nats url
	url := natsURL

	// creates a nats connection
	nc, err := nats.Connect(url)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", url)
		return
	}
	defer nc.Close()
	log.Printf("feeder: connects with nats server at : %s\n", url)

	Run(nc, liveboxID, liveboxFti, liveboxMac, liveboxLineID)

	// // create the SESSION stream
	// stream := session.CreateStream(nc)
	// _ = stream

	// // create the SESSION consumer
	// consumer := session.CreateConsumer(nc)
	// _ = consumer

	// //time.Sleep(1 * time.Second)

	// // create livebox info
	// lb := &model.LiveboxInfo{
	// 	ID:     liveboxID	,		// "LB1",
	// 	Fti:    liveboxFti	,		// "fti/abcdef",
	// 	Mac:    liveboxMac	,		// "mac",
	// 	LineID: liveboxLineID	,	// "LineID",
	// }
	// _ = lb

	// // context
	// ctx, cancel := context.WithCancel(context.Background())
	// defer cancel()
	// var wg sync.WaitGroup

	// _ = ctx

	// // create a Dora Feeder

	// // create the dora EventMaker
	// // em, err := dora.NewEventMaker(url)
	// // if err != nil {
	// // 	m := fmt.Sprintf("lbservices: EventMaker cannot start :%s", err.Error())
	// // 	panic(m)
	// // }
	// // defer em.Close()
	// // em.Publish("lbservices.dora.EventMaker", []byte("Dora EventMaker started"))

	// // starts the dora event maker
	// //wg.Add(1)
	// //go em.Run(ctx, &wg)

	// // create the lbservices
	// //s := res.NewService("lbservices")

	// // add dora resources
	// //dora.LiveboxDoraService(s)

	// // Start the service
	// //go s.ListenAndServe(url)

	// // start the heartbeat emiter
	// //e, _ := heartbeat.NewEmitter("lbservices", url, 5)
	// //e.Start()

	// // wait here
	// wg.Wait()

	// log.Printf("feeder: exit")
}

// Run start the feeder
func Run(nc *nats.Conn, id, fti, mac, line string) {

	// set the global nats url
	url := natsURL

	// creates a nats connection
	nc, err := nats.Connect(url)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", url)
		return
	}
	defer nc.Close()
	//log.Printf("feeder: connects with nats server at : %s\n", url)

	// create the SESSION stream
	stream := session.CreateStream(nc)
	_ = stream

	//time.Sleep(1 * time.Second)

	// create livebox info
	lb := &model.LiveboxInfo{
		ID:     liveboxID,     // "LB1",
		Fti:    liveboxFti,    // "fti/abcdef",
		Mac:    liveboxMac,    // "mac",
		LineID: liveboxLineID, // "LineID",
	}
	_ = lb

	// create the SESSION consumer
	subject := fmt.Sprintf("SESSION.%s.>", lb.ID) // eg SESSION.LB1.>
	consumer := session.CreateConsumer(nc, subject)
	_ = consumer

	// context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var wg sync.WaitGroup

	_ = ctx

	// create a Dora Feeder
	doraFeeder := session.NewDoraFeeder(nc, lb)
	// run it
	wg.Add(1)
	doraFeeder.Run(ctx)

	// create a dhcpV6 feeder
	dhcpV6Feeder := session.NewDhcpV6Feeder(nc, lb)
	// run it
	wg.Add(1)
	dhcpV6Feeder.Run(ctx)

	// create a applog feeder
	appLogFeeder := session.NewAppLogFeeder(nc, lb)
	// run it
	wg.Add(1)
	appLogFeeder.Run(ctx)

	// wait here
	wg.Wait()

	log.Printf("feeder: exit")

}
