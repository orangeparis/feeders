feeders


feeders is a session manager to handle livebox session

a livebox session collect all events concerning a unique livevox idenfied by an ID


usage:

Launch a uniq sesssion manager

    feeders --nats 127.0.0.1:4332

Send a request to session manager ( via resgate ) to start a specific livebox session

    call.session.new  { ID: "LIVEBOX_1234", Fti: "fti/abcdefgh", Mac: "00:00:00", LineID: "123456789"}

ie 
    POST /sesion/new { ID: "LIVEBOX_1234", Fti: "fti/abcdefgh", Mac: "00:00:00", LineID: "123456789"}


since there you can subscribe to the livebox events

    subscribe to SESSION.LIVEBOX_1234.>

and collect all events concerning this livebox

like 

    SESSION.LIVEBOX_1234.dora       ( event.livebox.fti.fti/abcdefgh.dora )
    SESSION.LIVEBOX_1234.dhcpv6     ( sniffer.N1.00:00:00.dhcpv6.> )
    SESSION.LIVEBOX_1234.applog     ( sniffer.log.123456789 )


