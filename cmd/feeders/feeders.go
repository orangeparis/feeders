package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	service "bitbucket.org/orangeparis/feeders/service"
	"github.com/nats-io/nats.go"
)

/*
	runs a feeders service

	listen to requests at

		get.session.ping"
		acces.session.ping

		access.session.new
		call.session.new

	emits heartbeat at

		Heartbeat.session
		Heartbeat.session.*


*/

var natsURL string

func main() {

	flag.StringVar(&natsURL, "nats", "nats://127.0.0.1:4222", "nats server url")

	flag.Parse()

	log.Printf("feeders: starting\n")

	// check parameters

	// set the global nats url
	url := natsURL

	// creates a nats connection
	nc, err := nats.Connect(url)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", url)
		return
	}
	defer nc.Close()
	log.Printf("feeders: connects with nats server at : %s\n", url)

	// context
	ctx := context.Background()

	// launch a session
	go service.Launch(ctx, nc)

	select {}

}
