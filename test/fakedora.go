package test

import (
	"context"
	"fmt"
	"time"

	model "bitbucket.org/orangeparis/feeders"
	"github.com/nats-io/nats.go"
)

type FakeDora struct {
	nc   *nats.Conn
	sink string
}

// NewFakeDora create a fake dora source message ( )
func NewFakeDora(nc *nats.Conn, lb *model.LiveboxInfo) (fk *FakeDora) {

	fk = &FakeDora{
		nc:   nc,
		sink: fmt.Sprintf("event.livebox.fti.%s.dora", lb.Fti),
	}
	return
}

// Run Starts DoraFeeder
func (x *FakeDora) Run(ctx context.Context) {
	fmt.Printf("fakeDora started\n")
	for {

		select {

		case <-ctx.Done():
			// shutdown
			fmt.Printf("fakeDora exit\n")
			return

		default:
			// emit a fake dora event message
			err := x.nc.Publish(x.sink, []byte("Fake Dora event"))
			if err != nil {
				fmt.Printf("fakeDora fail to publish message\n")
				return
			}
			fmt.Printf("fakeDora publish fake message\n")
			time.Sleep(1 * time.Second)

		}

	}
}
