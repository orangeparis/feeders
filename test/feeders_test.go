package test

import (
	"context"
	"fmt"
	"testing"
	"time"

	model "bitbucket.org/orangeparis/feeders"
	service "bitbucket.org/orangeparis/feeders/service"
	"github.com/nats-io/nats.go"
)

func TestFeeders(t *testing.T) {

	lb := &model.LiveboxInfo{
		ID:     "LB1",
		Fti:    "fti/abcdef",
		Mac:    "mac",
		LineID: "LineID",
	}
	_ = lb

	// creates nats connection
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", nats.DefaultURL)
		t.Fail()
		return
	}
	defer nc.Close()

	// create cancel function from context
	ctx, cancel := context.WithCancel(context.TODO())

	// launch a session manager
	go service.Launch(ctx, nc)

	// send request to start a livebox session

	// subscribe to SESSION.

	// send some sample events

	// cancel all goroutines for this context
	time.Sleep(10 * time.Second)
	cancel()
	time.Sleep(2 * time.Second)
	//nc.Close()
	println("Done.")

}
