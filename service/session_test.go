package session_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	service "bitbucket.org/orangeparis/feeders/service"
	"github.com/nats-io/nats.go"
)

func TestSession(t *testing.T) {

	// creates nats connection
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		fmt.Printf("cannot connect to %s\n", nats.DefaultURL)
		t.Fail()
		return
	}
	defer nc.Close()

	// create cancel function from context
	ctx, cancel := context.WithCancel(context.TODO())

	// launch a session
	go service.Launch(ctx, nc)

	time.Sleep(1 * time.Second)

	// test access.session.new
	result, err := nc.Request("access.session.new", []byte{}, 100*time.Millisecond)
	if err != nil {
		fmt.Printf("error calling access.session.new: %s\n", err.Error())
	} else {
		_ = result
		fmt.Printf("success calling access.session.new\n")
	}

	// test call.session.new
	c, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)

	rq := service.NewRequest{
		ID:     "LIVEBOX_1234",
		Fti:    "fti/abcdefgh",
		Mac:    "00:00:00",
		LineID: "123456789",
	}

	var response service.Result

	// test request new
	err = c.Request("call.session.new", rq, &response, 7*time.Second)
	if err != nil {
		fmt.Printf("error calling call.session.new: %s\n", err.Error())
		t.Fail()
		return
	} else {
		fmt.Printf("success calling call.session.new\n")
	}

	// check session ping
	r, err := nc.Request("get.session.ping", nil, 1*time.Second)
	_ = r
	if err != nil {
		fmt.Printf("error on get.session.ping: %s\n", err.Error())
		t.Fail()
		return
	}

	// check heartbreat
	sub, err := nc.SubscribeSync("Heartbeat.session")
	if err != nil {
		fmt.Printf("error on subscribe: %s\n", err.Error())
		t.Fail()
		return
	}

	m, err := sub.NextMsg(6 * time.Second)
	_ = m
	if err != nil {
		fmt.Printf("error on waiting for Hertbeat.session: %s\n", err.Error())
		t.Fail()
		return
	}

	time.Sleep(3 * time.Second)
	cancel()

	fmt.Println("Done")
}

// func TestSessionNew(t *testing.T) {

// 	// creates nats connection
// 	nc, err := nats.Connect(nats.DefaultURL)
// 	if err != nil {
// 		fmt.Printf("cannot connect to %s\n", nats.DefaultURL)
// 		t.Fail()
// 		return
// 	}
// 	defer nc.Close()

// 	// create cancel function from context
// 	ctx, cancel := context.WithCancel(context.TODO())

// 	// launch a session manager
// 	go service.Launch(ctx, nc)

// 	time.Sleep(1 * time.Second)

// 	cancel()

// }
