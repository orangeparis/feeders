package session

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	nats "github.com/nats-io/nats.go"
)

/*

	emits periodicaly heartbeat message on nats

	subject:   Heartbeat.<name>
	payload:   {"uptime":int}

*/

var RootTopic = "Heartbeat"
var DefaultPeriod int = 5

// Emitter : Heartbeat emiter  on subject Heartbeat.Name
type Emitter struct {
	*nats.Conn
	Topic  string        // root topic to identify Heartbeat event ( Heartbeat )
	Name   string        // name of the device : eg sniffer.S1
	Period time.Duration // eg 10 * time.Second

	started time.Time //
}

// Publish : publish mess	age on  sunbject Heartbeat.<Name>
func (p *Emitter) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	// publish to nats
	err = p.Conn.Publish(subject, message)
	return err
}

// CheckExists : check if a heartbeat already exists
func (p *Emitter) CheckExists() bool {

	subject := fmt.Sprintf("%s.%s", p.Topic, p.Name)
	wait := p.Period + 1*time.Second

	// wait for a heartbeat
	sub, err := p.SubscribeSync(subject)
	_, err = sub.NextMsg(wait)
	if err != nil {
		// timeout or error : does not exists return false
		fmt.Printf("Heartbeat: NO heartbeat detected for %s\n", subject)
		return false
	}
	// heartbeat already exists : return true
	fmt.Printf("Heartbeat: heartbeat detected for %s\n", subject)
	return true

}

// Start the timer
func (p *Emitter) Start(ctx context.Context) {

	go func() {

		for {
			now := time.Now()
			elapsed := now.Sub(p.started)
			message := fmt.Sprintf(`{"uptime":%d}`, int(elapsed.Seconds()))

			topic := p.Name
			p.Publish(topic, []byte(message))

			// wait for Done()
			select {
			case <-ctx.Done():
				return
			default:
				time.Sleep(p.Period)

			}

		}
	}()

}

// NewEmitter : create a Heartbeat Emitter
func NewEmitter(nc *nats.Conn, name string, period int) (*Emitter, error) {

	if period == 0 {
		period = DefaultPeriod // send every 5 seconds
	}
	x := strconv.Itoa(period)
	_ = x
	sPeriod, err := time.ParseDuration(strconv.Itoa(period) + "s")
	if err != nil {
		sPeriod, _ = time.ParseDuration("5s")
	}

	p := &Emitter{Conn: nc, Name: name, Topic: RootTopic, Period: sPeriod, started: time.Now()}

	p.Conn = nc

	msg := fmt.Sprintf("Heartbeat emitter %s created ", p.Name)
	log.Println(msg)

	return p, err
}
