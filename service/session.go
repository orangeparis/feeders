package session

import (
	"context"
	"fmt"

	model "bitbucket.org/orangeparis/feeders"
	session "bitbucket.org/orangeparis/feeders/pkg"

	"github.com/nats-io/nats.go"
)

/*

	create a session

	call.session.new  { liveboxID , mac, line, fti }



	const nats = require('nats').connect('nats://localhost:4222');

	nats.subscribe('get.example.model', (req, reply) => {
		nats.publish(reply, JSON.stringify({ result: { model: { message: "Hello, World!" }}}));
	});

	nats.subscribe('access.example.model', (req, reply) => {
		nats.publish(reply, JSON.stringify({ result: { get: true }}));
});


*/

type NewOut struct {
}

// Launch : starts a session
func Launch(ctx context.Context, nc *nats.Conn) {

	// start the heartbeat   heartbeat.session
	heartbeat, _ := NewEmitter(nc, "session", 5)
	heartbeat.Start(ctx)

	c, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)

	// pong service
	nc.Subscribe("get.session.ping", func(m *nats.Msg) {
		nc.Publish(m.Reply, []byte("PONG"))
	})
	nc.Subscribe("acces.session.ping", func(m *nats.Msg) {
		c.Publish(m.Reply, NewAccessResponse(true, "all"))
	})

	// handle acces.session.new : service for resgate
	c.Subscribe("access.session.new", func(m *nats.Msg) {
		//fmt.Printf("Received a message on access.session.new: %s\n", string(m.Data))
		c.Publish(m.Reply, NewAccessResponse(true, "all"))
	})

	// handle call.session.new
	c.Subscribe("call.session.new", func(_, reply string, p *NewRequest) {

		//fmt.Printf("received new request for livebox ID : %s\n", p.ID)
		err := LaunchLiveboxSession(ctx, nc, p)
		_ = err

		c.Publish(reply, successResponse{})

	})

	select {

	case <-ctx.Done():
		fmt.Printf("exiting session service\n")
		c.Close()

	}

}

// LaunchLiveboxSession : launch a livebox session  message contains livebox json parameters
func LaunchLiveboxSession(ctx context.Context, nc *nats.Conn, p *NewRequest) (err error) {

	// decode parameters
	lb := &model.LiveboxInfo{
		ID:     p.ID,
		LineID: p.LineID,
		Mac:    p.Mac,
		Fti:    p.Fti,
	}

	// check if session livebox already exists  ( Heartbeat.session.<LiveboxID> )
	id := p.ID
	heartbeat, _ := NewEmitter(nc, id, DefaultPeriod)
	if heartbeat.CheckExists() == true {
		// a session already exists for this livebox
		return nil
	}
	heartbeat.Start(ctx)

	//
	// create a livebox session
	//

	// create the SESSION consumer
	subject := fmt.Sprintf("SESSION.%s.>", id) // eg SESSION.LB1.>
	consumer := session.CreateConsumer(nc, subject)
	_ = consumer

	// create a Dora Feeder
	doraFeeder := session.NewDoraFeeder(nc, lb)
	// run it
	doraFeeder.Run(ctx)

	// create a dhcpV6 feeder
	dhcpV6Feeder := session.NewDhcpV6Feeder(nc, lb)
	// run it
	dhcpV6Feeder.Run(ctx)

	// create a applog feeder
	appLogFeeder := session.NewAppLogFeeder(nc, lb)
	// run it
	appLogFeeder.Run(ctx)

	err = nil
	return

}
