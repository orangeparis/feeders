package session

type successResponse struct {
	Result interface{} `json:"result"`
}

// type errorResponse struct {
// 	Error *Error `json:"error"`
// }

type accessResponse struct {
	Get  bool   `json:"get,omitempty"`
	Call string `json:"call,omitempty"`
}

// ResultData internal result data
type ResultData struct {
	Get  bool
	Post bool
}

// Result enveollpoe
type Result struct {
	Result ResultData
}

// NewResultOk return an OK Result
func NewAccessResponse(get bool, call string) successResponse {

	r := successResponse{
		Result: accessResponse{
			Get:  get,
			Call: call,
		},
	}

	return r

}

// NewRequest a livebox set of data
type NewRequest struct {
	ID     string // Livebox identifier
	Fti    string // fti
	Mac    string // mac
	LineID string // Line ID
}
