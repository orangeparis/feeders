module bitbucket.org/orangeparis/feeders

go 1.15

require (
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/nats-io/jsm.go v0.0.19
	github.com/nats-io/nats-server/v2 v2.1.8 // indirect
	github.com/nats-io/nats.go v1.10.1-0.20201013114232-5a33ce07522f
	google.golang.org/protobuf v1.25.0 // indirect
)
